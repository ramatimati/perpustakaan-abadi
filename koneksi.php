<?php
class database{
	public 	$host = "localhost",
			$username = "root",
			$pass = "",
			$db = "perpustakaan",
			$connect;

	function __construct(){
		$this->connect = new mysqli($this->host,$this->username,$this->pass,$this->db);
		if($this->connect->connect_errno){
			echo "Database Tidak Ada";
			exit;}
	}

//Transaksi dan temannya
	function inputTransaksi($id_transaksi, $nik, $nama, $telepon, $alamat, $id_buku, $tgl_sewa, $tgl_kembali, $id_peg){
		$simpan1 = "INSERT INTO transaksi VALUES('$id_transaksi','$id_buku','$tgl_sewa','$tgl_kembali','$id_peg','$nama','$nik','$telepon','$alamat')";
		$simpan2 = "INSERT INTO mahasiswa VALUES('$nik','$nama','$telepon','$alamat','$id_transaksi')";

		$hasil = $this->connect->query($simpan1);
		$hasil = $this->connect->query($simpan2);
	}

	function calon_cetak(){
		$data = "SELECT t.`id_transaksi`, m.`nama_mhs`, m.`no_hp`, m.`nik`, m.`alamat`,db.`id_buku`, db.`judul_buku`, db.`penulis`, t.`tgl_pinjam`, t.`tgl_kembali`, p.`nama_peg`
		FROM transaksi t, pegawai p, mahasiswa m, data_buku db
		WHERE t.`id_pegawai`=p.`id_pegawai`
		AND t.`id_buku`=db.`id_buku`
		AND t.`nik`=m.`nik`
		ORDER BY t.`id_transaksi` DESC LIMIT 1";
		$hasil = $this->connect->query($data);
		while ($d = mysqli_fetch_array($hasil)){
			$result[] = $d;
		}
		return $result;
	}

//Segalanya di tabel data_buku
	function tambah_buku($id_buku,$judul_buku,$penulis,$penerbit){
		$simpan = "INSERT INTO data_buku VALUES('$id_buku','$judul_buku','$penulis','$penerbit')";
		$hasil = $this->connect->query($simpan);
	}

	function tampil_buku(){
		$data = "SELECT * FROM data_buku";
		$hasil = $this->connect->query($data);
		while ($d = mysqli_fetch_array($hasil)){
			$result[] = $d;
		}
		return $result;
	}

	function hapus_buku($id_buku){
		$hapus = "DELETE FROM data_buku WHERE id_buku='$id_buku'";
		$hasil = $this->connect->query($hapus);
	}

	function edit_buku($id_buku){
		$data = "SELECT * FROM data_buku WHERE id_buku='$id_buku'";
		$hasil = $this->connect->query($data);
		while ($d = mysqli_fetch_array($hasil)){
			$result[] = $d;
		}
		return $result;
	}

	function update_buku($id_buku,$judul_buku,$penulis,$penerbit){
		$simpan = "UPDATE data_buku SET judul_buku='$judul_buku', penulis='$penulis', penerbit='$penerbit' WHERE id_buku='$id_buku'";
		$hasil = $this->connect->query($simpan);
	}

//Tempat berkumpul querynya para pegawai
	function tampil_peg(){
		$data = "SELECT * FROM pegawai";
		$hasil = $this->connect->query($data);
		while ($d = mysqli_fetch_array($hasil)){
			$result[] = $d;
		}
		return $result;
	}

	function tambah_peg($id_peg,$nama,$telepon,$alamat){
		$simpan = "INSERT INTO pegawai VALUES('$id_peg','$nama','$telepon','$alamat')";
		$hasil = $this->connect->query($simpan);
	}

	function hapus_peg($id_peg){
		$hapus = "DELETE FROM pegawai WHERE id_pegawai='$id_peg'";
		$hasil = $this->connect->query($hapus);
	}

//Kasihan tabel mahasiswa
	function tampil_mhs(){
		$data = "SELECT * FROM mahasiswa";
		$hasil = $this->connect->query($data);
		while ($d = mysqli_fetch_array($hasil)){
			$result[] = $d;
		}
		return $result;
	}

//Halaman Index.php disini
	function sh_tr(){
		$data = "SELECT t.`id_transaksi`, m.`nama_mhs`,m.`no_hp`, db.`judul_buku`, t.`tgl_pinjam`, t.`tgl_kembali`, p.`nama_peg`
				FROM transaksi t, pegawai p, mahasiswa m, data_buku db 
				WHERE t.`id_pegawai`=p.`id_pegawai`
				AND t.`id_buku`=db.`id_buku`
				AND t.`nik`=m.`nik`";
		$hasil = $this->connect->query($data);
		while ($d = mysqli_fetch_array($hasil)){
			$result[] = $d;
		}
		return $result;
	}

	
}
?>