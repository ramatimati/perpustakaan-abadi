<?php 
	include 'koneksi.php';
	$db = new database();
	ob_start();
?>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Cetak PDF</title>
</head>
<body onLoad="window.print();">
<!-- Nota Dong -->
			<div style="height: 390px; width: 1000px; background-color: #00000; border: solid 1px; margin-top: 10px; padding: 10px;">
				<?php foreach($db->calon_cetak() as $data){ ?>
							<table>
								<tr>
									<center>
										<div style="border: solid 3px; width: 150px;"> 
											Invoice No. <?php echo $data['id_transaksi'] ?>
										</div>
									</center>
								</tr>
							</table>
					<div class="row">
						<div class="col-lg-8">
							<table>
								<tr>
									<td>Nama</td>
									<td> : <?php echo $data['nama_mhs'] ?></td>
								</tr>
								<tr>
									<td>NIK</td>
									<td> : <?php echo $data['nik'] ?></td>
								</tr>
								<tr>
									<td>Alamat</td>
									<td> : <?php echo $data['alamat'] ?></td>
								</tr>
								<tr>
									<td>No. Telepon</td>
									<td> : <?php echo $data['no_hp'] ?></td>
								</tr>
							</table>
						</div>
						<div class="col-lg-4" align="right">
							<table>
								<h4 class="text-info">PERPUSTAKAAN ABADI</h4>
							</table><br><br><br><br>
						</div>
					</div>
					<center>
						<table border="solid 1px" width="800px">
							<tr align="center">
								<th>ID Buku</th>
								<th>Judul Buku</th>
								<th>Penulis</th>
								<th>Tanggal Sewa</th>
								<th>Tanggal Kembali</th>

							</tr>
							<tr>
								<td><?php echo $data['id_buku']; ?></td>
								<td><?php echo $data['judul_buku']; ?></td>
								<td><?php echo $data['penulis']; ?></td>
								<td><?php echo date('d-m-Y', strtotime($data['tgl_pinjam'])); ?></td>
								<td><?php echo date('d-m-Y', strtotime($data['tgl_kembali'])); ?></td>
							</tr>
						</table>
					</center>
					<div class="row">
						<div class="col-lg-8" style="font-size: 12px; padding-top: 70px;">
							*Harap membawa Nota ini saat mengembalika Buku <br>
							*Semua penyewa ajib patuh Tata Tertib yang berlaku di Perpustakaan ABADI
						</div>

						<div class="col-lg-4" style="padding-right: 100px;">
							<table align="right">
								<br>
								<tr align="center">
									<td>TTD</td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td><?php echo $data['nama_peg']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				<?php } ?>
			</div>
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>