<?php
include 'koneksi.php';
$db = new database();

	$aksi = $_GET['aksi'];

	if($aksi == "tambah_buku"){
		$db->tambah_buku(	$_POST['id_buku'],
							$_POST['judul_buku'],
							$_POST['penulis'],
							$_POST['penerbit']);
		header("location:databuku.php?id_buku=B-001&aksi=edit_buku");
	}

	elseif($aksi == "inputTransaksi"){
		$db->inputTransaksi(	$_POST['id_transaksi'],
								$_POST['nik'],
								$_POST['nama'],
								$_POST['telepon'],
								$_POST['alamat'],
								$_POST['id_buku'],
								date('Y-m-d', strtotime($_POST['tgl_sewa'])),
								date('Y-m-d', strtotime($_POST['tgl_kembali'])),
								$_POST['id_peg']);
		header("location:intransaksi.php");
	}

	elseif($aksi == "tambah_peg"){
		$db->tambah_peg(	$_POST['id_peg'],
							$_POST['nama'],
							$_POST['telepon'],
							$_POST['alamat']);
		header("location:pegawai.php");
	}

	elseif($aksi == "hapus_buku"){
		$db->hapus_buku($_GET['id_buku']);
		header("location:databuku.php?id_buku=B-001&aksi=edit_buku");
	}

	elseif($aksi == "hapus_peg"){
		$db->hapus_peg($_GET['id_peg']);
		header("location:pegawai.php");
	}

	// elseif($aksi == "edit_buku"){
	// 	$db->edit_buku(	$_POST['id_buku'],
	// 					$_POST['nama'],
	// 					$_POST['penulis'],
	// 					$_POST['penerbit']);
	// 	header("location:databuku.php");
	// }

	elseif($aksi == "update_buku"){
		$db->update_buku(	$_POST['id_buku'],
							$_POST['judul_buku'],
							$_POST['penulis'],
							$_POST['penerbit']);
		header("location:databuku.php?id_buku=B-001&aksi=edit_buku");
	}
?>
