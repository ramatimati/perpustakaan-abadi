<!DOCTYPE html>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Data Buku | Perpustakaan ABADI</title>
	<title>Data Buku | Perpustakaan ABADI</title>
	<style>
		.my-custom-scrollbar {
		position: relative;
		height: 470px;
		overflow: auto;
		}
		.table-wrapper-scroll-y {
		display: block;
		}
	</style>
</head>
<body>
<!-- Navbar dong -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
      <a class="navbar-brand" href="index.php">Perpustakaan ABADI</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Menu
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="intransaksi.php">Peminjaman Buku</a>
              <a class="dropdown-item" href="pegawai.php">Data Pegawai</a>
              <a class="dropdown-item" href="mahasiswa.php">Data Peminjam</a>
              <a class="dropdown-item" href="databuku.php">Data Buku</a>
            </div>
          </li>
        </ul>
      </div>
	</nav>
	<div class="container">
			<center><h3>Data Penyewa Perpustakaan ABADI</h3></center>
			<div class="table-wrapper-scroll-y my-custom-scrollbar">
			<table class="table table-bordered table-striped">
				<tr>
					<th>No</th>
					<th>NIK</th>
					<th>Nama Penyewa</th>
					<th>No. HP</th>
					<th>Alamat</th>
				</tr>
				<?php
				include 'koneksi.php';
				$db = new database();
				$no = "1";
				foreach($db->tampil_mhs() as $data){ ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $data['nik']; ?></td>
						<td><?php echo $data['nama_mhs']; ?></td>
						<td><?php echo $data['no_hp']; ?></td>
						<td><?php echo $data['alamat']; ?></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>