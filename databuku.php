<!DOCTYPE html>
<?php
	include 'koneksi.php';
	$db = new database();
?>
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Data Buku | Perpustakaan ABADI</title>
	<style>
		.my-custom-scrollbar {
		position: relative;
		height: 470px;
		overflow: auto;
		}
		.table-wrapper-scroll-y {
		display: block;
		}
	</style>
</head>
<body>
<!-- Navbar dong -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
      <a class="navbar-brand" href="index.php">Perpustakaan ABADI</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Menu
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="intransaksi.php">Peminjaman Buku</a>
              <a class="dropdown-item" href="pegawai.php">Data Pegawai</a>
              <a class="dropdown-item" href="mahasiswa.php">Data Peminjam</a>
              <a class="dropdown-item" href="databuku.php">Data Buku</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
<center><h1>PERPUSTAKAAN ABADI</h1></center>
<div class="row m-0 p-0">
<!-- bagian kiri -->
	<div class="col-lg-3">
		<h4>Input Data Buku</h4>
			<form action="proses.php?aksi=tambah_buku" method="post" class="form-group" novalidate="">
				<table>
					<tr>
						<td>ID Buku</td>
						<td><input type="text" required name="id_buku" class="form-control" placeholder="B-001"></td>
					</tr>
					<tr>
						<td>Judul Buku</td>
						<td><input type="text" name="judul_buku" class="form-control"></td>
					</tr>
					<tr>
						<td>Penulis</td>
						<td><input type="text" name="penulis" class="form-control"></td>
					</tr>
					<tr>
						<td>Penerbit</td>
						<td><input type="text" name="penerbit" class="form-control"></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="Simpan" class="btn btn-outline-info"></td>
					</tr>
				</table>
			</form>
			<h4>Edit Data Buku</h4>
	<?php 
		foreach($db->edit_buku($_GET['id_buku']) as $data){
	?>
	<form action="proses.php?aksi=update_buku" method="post" novalidate="">
		<table>
			<tr>
				<td>ID Buku</td>
				<td><input class="form-control" required type="text" name="id_buku" value="<?php echo $data['id_buku'] ?>"></td>
			</tr>
			<tr>
				<td>Judul Buku</td>
				<td><input class="form-control" required type="text" name="judul_buku" value="<?php echo $data['judul_buku'] ?>"></td>
			</tr>
			<tr>
				<td>Penulis</td>
				<td><input class="form-control" required type="text" name="penulis" value="<?php echo $data['penulis'] ?>"></td>
			</tr>
			<tr>
				<td>Penerbit</td>
				<td><input class="form-control" required="required" type="text" name="penerbit" value="<?php echo $data['penerbit'] ?>"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Simpan" class="btn btn-outline-info"></td>
			</tr>
		</table>
	</form>
	<?php } ?>
		</div>

<!-- bagian kanan -->
	<div class="col-lg-9">
		<h4>Data Buku</h4>
		<div class="table-wrapper-scroll-y my-custom-scrollbar">
			<table class="table table-bordered table-striped">
				<tr>
					<th>No</th>
					<th>ID Buku</th>
					<th>Judul Buku</th>
					<th>Penulis</th>
					<th>Penerbit</th>
					<th>Aksi</th>
				</tr>
				<?php
				$no = "1";
				foreach($db->tampil_buku() as $data){ ?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $data['id_buku']; ?></td>
						<td><?php echo $data['judul_buku']; ?></td>
						<td><?php echo $data['penulis']; ?></td>
						<td><?php echo $data['penerbit']; ?></td>
						<td><a href="databuku.php?id_buku=<?php echo $data['id_buku'] ?>&aksi=edit_buku">Edit</a>
							<a href="proses.php?id_buku=<?php echo $data['id_buku'] ?>&aksi=hapus_buku">Hapus</a></td>
					</tr>
				<?php } ?>
			</table>
		</div>
	</div>
</div>

	

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    	<script src="jquery-1.11.3.js"></script>
		<script type="text/javascript">
			function isi_otomatis(){
				var idbuku = $("#idbuku").val();
				$.ajax({
					url: 'proses.php' ,
					data: "idbuku="+idbuku ,
				}).success(function (data) {
					var json = data,
					obj = JSON.parse(json);
					$('#judul_buku').val(obj.judulbuku);
					$('#penulis').val(obj.penulis);
					$('#penerbit').val(obj.penerbit);
				});
			}
		</script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>