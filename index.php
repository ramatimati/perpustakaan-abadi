<!doctype html>
<?php
  include 'koneksi.php';
  $db = new database();
?>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
      .my-custom-scrollbar {
      position: relative;
      height: 470px;
      overflow: auto;
      }
      .table-wrapper-scroll-y {
      display: block;
      }
    </style>

    <title>Perpustakaan ABADI</title>
  </head>
  <body>
<!-- Navbar dong -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
      <a class="navbar-brand" href="index.php">Perpustakaan ABADI</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Menu
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="intransaksi.php">Peminjaman Buku</a>
              <a class="dropdown-item" href="pegawai.php">Data Pegawai</a>
              <a class="dropdown-item" href="mahasiswa.php">Data Peminjam</a>
              <a class="dropdown-item" href="databuku.php?id_buku=B-001&aksi=edit_buku">Data Buku</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>

  <div class="row m-0 p-0">
<!-- bagian kiri -->
    <div class="col-lg-3">
        <h1>Menu</h1>
        <div class="btn-group-vertical" style="width: 100%;">
          <a href="intransaksi.php" style="text-decoration: none; text-align: left;" class="btn btn-outline-info">PEMINJAMAN BUKU</a>
          <a href="pegawai.php" style="text-decoration: none; text-align: left;" class="btn btn-outline-info">DATA PEGAWAI</a>
          <a href="mahasiswa.php" style="text-decoration: none; text-align: left;" class="btn btn-outline-info">DATA PEMINJAM</a>
          <a href="databuku.php?id_buku=B-001&aksi=edit_buku" style="text-decoration: none; text-align: left;" class="btn btn-outline-info">DATA BUKU</a>
        </div>
    </div>
    <div class="col-lg-9" style="margin-top: 20px;">
      <h4>Data Transaksi</h4>
      <div class="table-wrapper-scroll-y my-custom-scrollbar">
        <table class="table table-bordered table-striped">
            <tr>
              <th>ID Transaksi</th>
              <th>Penyewa</th>
              <th>No. Telepon</th>
              <th>Judul Buku</th>
              <th>Tanggal Sewa</th>
              <th>Tanggal Kembali</th>
              <th>Pegawai</th>
            </tr>
              <?php foreach($db->sh_tr() as $data){ ?>
            <tr>
              <td><?php echo $data['id_transaksi']; ?></td>
              <td><?php echo $data['nama_mhs']; ?></td>
              <td><?php echo $data['no_hp']; ?></td>
              <td><?php echo $data['judul_buku']; ?></td>
              <td><?php echo date('d-m-Y', strtotime($data['tgl_pinjam'])); ?></td>
              <td><?php echo date('d-m-Y', strtotime($data['tgl_kembali'])); ?></td>
              <td><?php echo $data['nama_peg']; ?></td>
            </tr>
              <?php } ?>
        </table>
      </div>
    </div>


<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>