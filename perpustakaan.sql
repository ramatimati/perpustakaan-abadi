/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.4.6-MariaDB : Database - perpustakaan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `data_buku` */

DROP TABLE IF EXISTS `data_buku`;

CREATE TABLE `data_buku` (
  `id_buku` varchar(15) NOT NULL,
  `judul_buku` varchar(30) NOT NULL,
  `penulis` varchar(30) NOT NULL,
  `penerbit` varchar(30) NOT NULL,
  PRIMARY KEY (`id_buku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_buku` */

insert  into `data_buku`(`id_buku`,`judul_buku`,`penulis`,`penerbit`) values ('B-003','Mengalir Seperti Air','Maulana Zain','Purnama'),('B-004','Berpikir Seperti Kambing','Bagas Pratama','Hiya'),('B-005','Hakekat Beribadah','Yudha Kristianto','Penerbit Grogol'),('B-006','Tafsir Ngawur','Nanang Terpandang Kondang','Sidomulyo Abadi'),('B-007','Setelah Hari Akhir','Nanang Terpandang Kondang','Sidomulyo Abadi'),('B-008','Aku Pemersatu Umat?','Nanang Terpandang Kondang','Sidomulyo Abadi'),('B-009','Hahaha','Ramadhani Syam','Mediakita'),('B-010','Hari yang Baik untuk Meninggal','Nurhadi','MahaAsyik'),('B-011','Menjadi Manusia','Yudha Kristianto',' Elex Media Komputindo');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `nik` varchar(30) NOT NULL,
  `nama_mhs` varchar(30) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `id_transaksi` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`nik`,`nama_mhs`,`no_hp`,`alamat`,`id_transaksi`) values ('357100991312001','Rifqi Ramadhan','098767678888','Mojoroto','005'),('35711212100003','Bacharudin','081321236765','Makasar','0003'),('35711512990002','Rifqi Ramadhan','085334302440','Bandar','0001'),('35717330111002','rara sekar','00000000','panti asuhan','0002');

/*Table structure for table `pegawai` */

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(15) NOT NULL,
  `nama_peg` varchar(30) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pegawai` */

insert  into `pegawai`(`id_pegawai`,`nama_peg`,`no_hp`,`alamat`) values ('P1','Fokusboy1','098767678888','Bandar Lor'),('P2','Fokusboy2','082312340987','Bangsongan'),('P3','Rifqi Ramadhan','098767678888','panti asuhan');

/*Table structure for table `transaksi` */

DROP TABLE IF EXISTS `transaksi`;

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(5) NOT NULL,
  `id_buku` varchar(5) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `id_pegawai` varchar(5) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `nik` varchar(25) DEFAULT NULL,
  `telepon` varchar(12) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `transaksi` */

insert  into `transaksi`(`id_transaksi`,`id_buku`,`tgl_pinjam`,`tgl_kembali`,`id_pegawai`,`nama`,`nik`,`telepon`,`alamat`) values ('0001','B-002','1970-01-01','1970-01-01','P1','Rifqi Ramadhan','35711512990002','085334302440','Bandar'),('0002','B-009','2019-10-02','2019-10-17','P1','rara sekar','35717330111002','00000000','panti asuhan'),('0003','B-004','2019-10-24','2019-11-01','P2','Bacharudin','35711212100003','081321236765','Makasar'),('0004','B-002','2019-10-02','2019-10-18','P1','sukma','35717330111002','098767678888','Mojoroto'),('005','B-010','2019-10-02','2019-10-11','P2','Rifqi Ramadhan','357100991312001','098767678888','Mojoroto'),('006','B-009','2019-11-01','2019-11-07','P1','Ahmad InginLana','35717330111002','12413513','Mojoroto');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
