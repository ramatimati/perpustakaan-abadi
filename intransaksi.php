<?php 
	include 'koneksi.php';
	$db = new database();
?>

<!doctype html>
<html lang="en">
<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Peminjaman Buku | Perpustakaan ABADI</title>
</head>
<body>
<!-- Navbar dong -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-info">
      <a class="navbar-brand" href="index.php">Perpustakaan ABADI</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Menu
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="intransaksi.php">Peminjaman Buku</a>
              <a class="dropdown-item" href="pegawai.php">Data Pegawai</a>
              <a class="dropdown-item" href="mahasiswa.php">Data Peminjam</a>
              <a class="dropdown-item" href="databuku.php">Data Buku</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>


	<div class="row m-0 p-0">
<!-- form pinjam -->
		<div class="col-lg-3">
			<h4>FORM PEMINJAMAN BUKU</h4>
				<form action="proses.php?aksi=inputTransaksi" method="post">
					<table>
						<tr>
							<td>ID Transaksi</td>
							<td><input type="text" name="id_transaksi" class="form-control" placeholder="0001"></td>
						</tr>
						<tr>
							<td>NIK</td>
							<td><input type="text" name="nik" class="form-control"></td>
						</tr>
						<tr>
							<td>Nama</td>
							<td><input type="text" name="nama" class="form-control"></td>
						</tr>
						<tr>
							<td>No. Telepon</td>
							<td><input type="text" name="telepon" class="form-control"></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><input type="text" name="alamat" class="form-control"></td>
						</tr>
						<tr>
							<td>ID Buku</td>
							<td><input type="text" name="id_buku" class="form-control" placeholder="B-001"></td>
						</tr>
						<tr>
							<td>Tanggal Sewa</td>
							<td><input type="date" name="tgl_sewa" class="form-control"></td>
						</tr>
						<tr>
							<td>Tanggal Kembali</td>
							<td><input type="date" name="tgl_kembali" class="form-control"></td>
						</tr>
						<tr>
							<td>ID Pegawai</td>
							<td><select name="id_peg" class="form-control">
									  <option value="P1">P1</option>
									  <option value="P2">P2</option>
									</select>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" class="btn btn-outline-info"></td>
						</tr>
					</table>
				</form>
		</div>
		
		<div class="col-lg-9">
<!-- Nota Dong -->
			<div style="height: 390px; width: 1000px; background-color: #00000; border: solid 1px; margin-top: 10px; padding: 10px;">
				<?php foreach($db->calon_cetak() as $data){ ?>
							<table>
								<tr>
									<center>
										<div style="border: solid 3px; width: 150px;"> 
											Invoice No. <?php echo $data['id_transaksi'] ?>
										</div>
									</center>
								</tr>
							</table>
					<div class="row">
						<div class="col-lg-8">
							<table>
								<tr>
									<td>Nama</td>
									<td> : <?php echo $data['nama_mhs'] ?></td>
								</tr>
								<tr>
									<td>NIK</td>
									<td> : <?php echo $data['nik'] ?></td>
								</tr>
								<tr>
									<td>Alamat</td>
									<td> : <?php echo $data['alamat'] ?></td>
								</tr>
								<tr>
									<td>No. Telepon</td>
									<td> : <?php echo $data['no_hp'] ?></td>
								</tr>
							</table>
						</div>
						<div class="col-lg-4" align="right">
							<table>
								<h4 class="text-info">PERPUSTAKAAN ABADI</h4>
							</table><br><br><br><br>
						</div>
					</div>
					<center>
						<table border="solid 1px" width="800px">
							<tr align="center">
								<th>ID Buku</th>
								<th>Judul Buku</th>
								<th>Penulis</th>
								<th>Tanggal Sewa</th>
								<th>Tanggal Kembali</th>

							</tr>
							<tr>
								<td><?php echo $data['id_buku']; ?></td>
								<td><?php echo $data['judul_buku']; ?></td>
								<td><?php echo $data['penulis']; ?></td>
								<td><?php echo date('d-m-Y', strtotime($data['tgl_pinjam'])); ?></td>
								<td><?php echo date('d-m-Y', strtotime($data['tgl_kembali'])); ?></td>
							</tr>
						</table>
					</center>
					<div class="row">
						<div class="col-lg-8" style="font-size: 12px; padding-top: 70px;">
							*Harap membawa Nota ini saat mengembalika Buku <br>
							*Semua penyewa ajib patuh Tata Tertib yang berlaku di Perpustakaan ABADI
						</div>

						<div class="col-lg-4" style="padding-right: 100px;">
							<table align="right">
								<br>
								<tr align="center">
									<td>TTD</td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td><br></td>
								</tr>
								<tr>
									<td><?php echo $data['nama_peg']; ?></td>
								</tr>
							</table>
						</div>
					</div>
				<?php } ?>

			</div>
		<a href="pdf.php" style="float: left; margin-top: 10px;">
			<button class="btn btn-outline-info">Cetak Nota</button>
		</a>
		</div>
	</div>

<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>